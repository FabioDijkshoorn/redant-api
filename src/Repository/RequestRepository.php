<?php

namespace App\Repository;

use App\Entity\Request;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Request|null find($id, $lockMode = null, $lockVersion = null)
 * @method Request|null findOneBy(array $criteria, array $orderBy = null)
 * @method Request[]    findAll()
 * @method Request[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Request::class);
    }

    public function findAllReverse($max, $first, $type)
    {
        return $this->createQueryBuilder('r')
            ->select('r.id', 'r.type', 'r.status', 'r.api', 'r.request', 'r.createdAt', 'r.response_time')
            ->where('r.type = :type')
            ->setParameter('type', $type)
            ->orderBy('r.id', 'DESC')
            ->setFirstResult($first)
            ->setMaxResults($max)
            ->getQuery()->getResult();
    }

    public function getAverageResponseTime($date, $today, $dateType)
    {
        return $this->createQueryBuilder('r')
            ->select($dateType,'ROUND(AVG(r.response_time), 0) as value', 'r.api')
            ->where('r.createdAt BETWEEN :date AND :today')
                ->setParameter('today', $today)
                ->setParameter('date', $date)
            ->groupBy('date')
            ->addGroupBy('r.api')
            ->getQuery()->getResult();
    }

    public function findUnique($field)
    {
        return $this->createQueryBuilder('r')
            ->select($field)->distinct(true)
            ->getQuery()->getResult();
    }

    public function findApiTimewindow($date, $today, $api)
    {
        return $this->createQueryBuilder('r')
            ->select('r.api')
            ->where('r.createdAt BETWEEN :date AND :today')
            ->andWhere('r.api = :api')
            ->setParameter('api', $api)
            ->setParameter('today', $today)
            ->setParameter('date', $date)
            ->getQuery()->getResult();
    }
    
    // /**
    //  * @return Request[] Returns an array of Request objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Request
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
