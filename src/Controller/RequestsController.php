<?php


namespace App\Controller;


use App\Entity\Request;
use App\Repository\RequestRepository;
use Rompetomp\InertiaBundle\Service\InertiaInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RequestsController extends AbstractController
{
    /**
     * @Route("/requests/{type}", name="request.index")
     *
     * @param string $type
     * @param InertiaInterface $inertia
     * @param RequestRepository $repository
     * @return Response
     */
    public function index(string $type, InertiaInterface $inertia, RequestRepository $repository)
    {
        return $inertia->render('requestComponents/index',['type' => $type]);
    }

    /**
     * @Route("/requests/{type}/{page}/{max}", name="requests.get")
     * @param int $page
     * @param int $max
     * @param RequestRepository $repository
     * @return JsonResponse
     */

    public function getRequests(string $type, int $page, int $max, RequestRepository $repository)
    {
        if ($page === 1) {
            $prevPage = null;
            $first = 0;
        } else {
            $prevPage = sprintf("http://127.0.0.1:8000/requests/%d/%d", ($page - 1), $max);
            $first = ($page - 1) * $max;
        }

        $data = $repository->findAllReverse($max, $first, $type);
        $rows = count($repository->findBy(array('type' => $type)));
        $response = new JsonResponse(['data' => [
            'total' => $rows,
            'per_page' => $max,
            'current_page' => $page,
            //How many times the maximum fits inside the counted $rows + 1
            'last_page' => (($rows - ($rows % $max)) / $max + 1),
            "prev_page_url" => $prevPage,
            "path" => "http://127.0.0.1:8000/requests/",
            "from" => $first,
            "to" => ($first + $max),
            "data" => $data
        ]]);

        return $response;
    }

    /**
     * @Route("/request/info/{id}", name="request.info")
     *
     * @param RequestRepository $repository
     *
     * @return Response
     */
    public function getRequestInfo(Request $id, RequestRepository $repository)
    {
        $response = new JsonResponse(['data' => [
            'type' => $id->getType(),
            'data' => $id->getData(),
            'responseTime' => $id->getResponseTime(),
            'api' => $id->getApi()
        ]

        ]);

        return $response;
    }

    /**
     * @Route("/apis/info", name="apis.info")
     * @param RequestRepository $repository
     * @return JsonResponse
     * @throws \Exception
     */
    public function getApisInfo(RequestRepository $repository)
    {
        $today = new \DateTime();
        $dateMonth = new \DateTime('first day of this month 00:00:00');
        $dateDay = new \DateTime("00:00:00");
        $apis = $repository->findUnique('r.api');
        $requestsPerApi = [];

        foreach ($apis as $api) {
            $requestsPerApi[$api['api']] = [
                'requestsTotal' => count($repository->findBy($api)),
                'requestsPerMonth' => count($repository->findApiTimewindow($dateMonth, $today, $api)),
                'requestsPerDay' => count($repository->findApiTimewindow($dateDay, $today, $api))
            ];
        }


        return new JsonResponse(['data' => [
            'data' => $requestsPerApi,
            'apis' => $apis
        ]]);
    }
}