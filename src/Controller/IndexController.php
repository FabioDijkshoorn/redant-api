<?php


namespace App\Controller;


use App\Repository\APIRepository;
use App\Repository\RequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Rompetomp\InertiaBundle\Service\InertiaInterface;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function index(InertiaInterface $inertia, APIRepository $repository)
    {
        $data = $repository->findAllWithoutId();


        return $inertia->render('index', ['name' => 'RedAnt', 'data' => $data]);
    }

    /**
     * @Route("/chart/{timewindow}", name="chart")
     * @param int $timewindow
     * @param RequestRepository $repository
     * @return JsonResponse
     * @throws \Exception
     */
    public function chart(int $timewindow, RequestRepository $repository)
    {
        $today = new \DateTimeImmutable();

        if ($timewindow === 1) {
            $date = $today->modify("-1 hour");
            $dateType = "DATE_FORMAT(r.createdAt, '%d-%m-%Y %H:%i:00') AS date";
        } elseif ($timewindow === 2) {
            $date = $today->modify("-1 day");
            $dateType = "DATE_FORMAT(r.createdAt, '%m-%d-%Y %H:00:00') AS date";
        } elseif ($timewindow === 3) {
            $date = $today->modify("-7 day");
            $dateType = "DATE_FORMAT(r.createdAt, '%m-%d-%Y') AS date";
        } else {
            $date = $today->modify("-1 month");
            $dateType = "DATE_FORMAT(r.createdAt, '%m-%d-%Y') AS date";
        }

        $queryData = $repository->getAverageResponseTime ($date, $today, $dateType);
        $apis = $repository->findUnique('r.api');
        $data = [];
        $i = 0;
        foreach($queryData as $apiData) {
            if (!isset($data[$apiData['date']])) {
                $data[$apiData['date']] = [
                    'date' => $apiData['date']
                ];
            }
            $data[$apiData['date']][$apiData['api']] = $apiData['value'];
        }
        $data = array_values($data);

        return new JsonResponse(['data' => [
            'data' => $data,
            'apis' => $apis
        ]]);
    }

}