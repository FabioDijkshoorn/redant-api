# RedAnt-API
There is currently an issue with the scripts not loading when accessing the website through apache.
To bypass this issue for now, start a Symfony server through the terminal.

## To view the website do the following:
- `composer install`
- `yarn install`
- `sf server:start`